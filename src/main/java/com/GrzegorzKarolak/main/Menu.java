package com.GrzegorzKarolak.main;

import com.GrzegorzKarolak.assistants.*;
import com.GrzegorzKarolak.serialize.Serialize;

import java.util.Scanner;


/**
 *  główne menu programu
 */

public interface Menu {


    static void mainMenu() {
        System.out.printf("%nWitaj w programie AdminSped.%n");
        System.out.println("Wybierz co chcesz zrobić:");
        System.out.printf("'1' - Menu aut%n'2' - Menu spedytorów%n'3' - Menu kierowców%n'4' - Menu przewoźników%n'5' - Menu klientów%n'6' - Menu zleceń transportowych%n'7' - Menu tras przewoźników%n'8' - Wyjście%n");
        try {
            Scanner in = new Scanner(System.in);
            int inputInt = in.nextInt();
            switch (inputInt) {
                case 1:
                    new VehicleAssistant().vehicleMenu();
                    mainMenu();
                    break;
                case 2:
                    new ForwarderAssistant().forwarderMenu();
                    mainMenu();
                    break;
                case 3:
                    new DriverAssistant().driverMenu();
                    mainMenu();
                    break;
                case 4:
                    new CarrierAssistant().carrierMenu();
                    mainMenu();
                    break;
                case 5:
                    new CustomerAssistant().customerMenu();
                    mainMenu();
                    break;
                case 6:
                    new TransportOrderAssistant().transportOrderMenu();
                    mainMenu();
                    break;
                case 7:
                    new RouteAssistant().routeMenu();
                    mainMenu();
                    break;
                case 8:
                    Serialize.save();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Wybierz nr od 1 do 9");
                    mainMenu();
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 9");
            mainMenu();
        }
    }
}

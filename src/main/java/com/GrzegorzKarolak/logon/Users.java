//package com.GrzegorzKarolak.logon;
//
//import com.GrzegorzKarolak.main.Main;
//
//import java.io.Console;
//import java.io.Serializable;
//import java.util.Objects;
//import java.util.Scanner;
//
//public class Users implements Serializable {
//
//    //Czemu nie działa logowanie na usersów poza 'admin'?
//
//
//    private String user;
//    private char[] password;
//    private String passwordString;
//    private boolean admin;
//
//
//    public Users() {
//        addUser();
//    }
//    public Users (String u, String p, boolean a){
//        this.user = u;
//        this.passwordString = p;
//        this.admin = a;
//    }

//    public void adminMenu() {
//        System.out.println("Witaj w menu administratora.");
//        System.out.println("Wybierz co chcesz zrobić:");
//        System.out.printf("'1' - Dodanie użytkownika%n'2' - Usunięcie użytkownika%n'3' - Wyświetlenie wszystkich użytkowników%n'4' - Przejście do głównego menu%n");
//        try {
//            Scanner in = new Scanner(System.in);
//            int inputInt = in.nextInt();
//            switch (inputInt) {
//                case 1:
//                    Main.getUsers().add(new Users());
//                    break;
//                case 2:
//                    removeUser();
//                    break;
//                case 3:
//                    showAll();
//                    break;
//                case 4:
//                    Main.mainMenu();
//                    break;
//                default:
//                    System.out.println("Wybierz nr od 1 do 4");
//                    break;
//            }
//        } catch (java.util.InputMismatchException e) {
//            System.out.println("Wybierz nr od 1 do 4");
//        }
//
//        adminMenu();
//    }

//    public void login() {
//        Console in = System.console();
//        System.out.println("Podaj nazwę użytkownika:");
//        String loginUser = in.readLine();
//        for (Users i : Main.getUsers()) {
//            if (i.user.equals(loginUser)) {
//                System.out.println("Podaj hasło:");
//                char[] loginPassword = in.readPassword();
//                String loginPasswordString = String.valueOf(loginPassword);
//                if (i.passwordString.equals(loginPasswordString) && i.admin == false) {
//                    Main.mainMenu();
//                } else if (i.passwordString.equals(loginPasswordString) && i.admin == true) {
//                    adminMenu();
//                } else {
//                    System.out.println("Błędne hasło.");
//                    login();
//                }
//            }else {
//                System.out.println("Podaj prawidłową nazwę użytkownika.");
//                login();
//            }
//        }
//}

//    private void addUser() {
//        Console in = System.console();
//        System.out.println("Podaj nową nazwę użytkownika:");
//
//        this.user = in.readLine();
//        System.out.println("Podaj hasło:");
//        this.password = in.readPassword();
//        this.passwordString = String.valueOf(password);
//
//        System.out.println("Czy użytkownik ma mieć uprawnienia administratora?");
//        System.out.println("Wpisz: 'TAK' lub 'NIE'");
//        Scanner input = new Scanner(System.in);
//        String inputString = input.nextLine();
//        boolean done = false;
//        while (!done) {
//            if (inputString.equalsIgnoreCase("TAK")) {
//                this.admin = true;
//                done = true;
//            } else if (inputString.equalsIgnoreCase("NIE")) {
//                this.admin = false;
//                done = true;
//            } else {
//                System.out.println("Wpisz 'TAK' lub 'NIE'");
//                input = new Scanner(System.in);
//            }
//        }
//        Main.getUsers().add(this);
//        System.out.println("Utorzono użytkownika " + this.user);
//        Main.save();
//        adminMenu();
//    }

//    private void removeUser() {
//        Console in = System.console();
//        System.out.println("Podaj nazwę użytkownika, którego chcesz usunąć:");
//        String deleteUser = in.readLine();
//        for (Users i : Main.getUsers()){
//            if (i.user.equals(deleteUser)){
//                System.out.println("Usunięto użytkowinika:" + i.user);
//                Main.getUsers().remove(i);
//            } else {
//                System.out.println("Brak takiego użytkownika");
//            }
//        }
//        adminMenu();
//    }
//
//    private void showAll() {
//
//        for (Users i : Main.getUsers()) {
//            Main.starsUp();
//
//            System.out.println("Nazwa użytkownika: " + i.user);
//            System.out.print("Uprawnienia administratora: ");
//            if (i.admin) {
//                System.out.println("TAK");
//            } else {
//                System.out.println("NIE");
//            }
//            System.out.println(" ");
//            Main.starsBottom();
//        }
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Users users = (Users) o;
//        return Objects.equals(user, users.user);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(user);
//    }
//}

package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.assistants.RouteAssistant;
import com.GrzegorzKarolak.interfaces.Descriptive;
import com.GrzegorzKarolak.interfaces.Mutable;
import com.GrzegorzKarolak.main.Signs;
import com.GrzegorzKarolak.serialize.Serialize;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Scanner;

public class Route implements Serializable, Mutable, Descriptive {

    private int idRoute;
    private Driver driver;
    private Vehicle vehicle;
    private Forwarder forwarder;
    private TransportOrder transportOrder;
    private LocalDate startDate;
    private LocalDate endDate;
    private double penalty;
    private String reasonOfPenalty;
    private String otherRoute;
    private static int countRoute = 7000000;
    private Carrier carrier;
    private int kmPerTrip;
    private int [] numbers = inputDate();
    private boolean penaltY;
    private double costs;
    private int paymentPeriod;
    private String kindOfCargo;
    private boolean paid;
    private double profit;

    public int getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(int idRoute) {
        this.idRoute = idRoute;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Forwarder getForwarder() {
        return forwarder;
    }

    public void setForwarder(Forwarder forwarder) {
        this.forwarder = forwarder;
    }

    public TransportOrder getTransportOrder() {
        return transportOrder;
    }

    public void setTransportOrder(TransportOrder transportOrder) {
        this.transportOrder = transportOrder;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(double penalty) {
        this.penalty = penalty;
    }

    public String getReasonOfPenalty() {
        return reasonOfPenalty;
    }

    public void setReasonOfPenalty(String reasonOfPenalty) {
        this.reasonOfPenalty = reasonOfPenalty;
    }

    public String getOtherRoute() {
        return otherRoute;
    }

    public void setOtherRoute(String otherRoute) {
        this.otherRoute = otherRoute;
    }

    public static int getCountRoute() {
        return countRoute;
    }

    public static void setCountRoute(int countRoute) {
        Route.countRoute = countRoute;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public int getKmPerTrip() {
        return kmPerTrip;
    }

    public void setKmPerTrip(int kmPerTrip) {
        this.kmPerTrip = kmPerTrip;
    }

    public boolean isPenalltY() {
        return penaltY;
    }

    public void setPenalltY(boolean penalltY) {
        this.penaltY = penalltY;
    }

    public double getCosts() {
        return costs;
    }

    public void setCosts(double costs) {
        this.costs = costs;
    }

    public int getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(int paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }

    public String getKindOfCargo() {
        return kindOfCargo;
    }

    public void setKindOfCargo(String kindOfCargo) {
        this.kindOfCargo = kindOfCargo;
    }

    public boolean isPenaltY() {
        return penaltY;
    }

    public void setPenaltY(boolean penaltY) {
        this.penaltY = penaltY;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }


    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public Route() {

        this.setIdRoute(getCountRoute());
        setCountRoute(getCountRoute() + 1);
        Scanner in = new Scanner(System.in);
        boolean done = false;
        int inputInt = 0;
        double inputDouble = 0;
        String inputString;
        while (!done) {
            try {
                System.out.println("Podaj ID zlecenia transportowego realizowanego na trasie:");
                inputInt = in.nextInt();
                TransportOrder to = Serialize.getTransportOrders().get(inputInt - 6000000);
                setTransportOrder(to);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                System.out.println("Wpiszcz prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        while (!done) {
            try {
                System.out.println("Podaj ID spedytora obsługującego trasę:");
                inputInt = in.nextInt();
                Forwarder f = Serialize.getForwarders().get(inputInt - 2000000);
                setForwarder(f);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                System.out.println("Wpiszcz prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        while (!done) {
            try {
                System.out.println("Podaj ID kierowcy realizującego trasę:");
                inputInt = in.nextInt();
                Driver d = Serialize.getDrivers().get(inputInt - 3000000);
                setDriver(d);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                System.out.println("Wpiszcz prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        while (!done) {
            try {
                System.out.println("Podaj ID auta realizującego trasę:");
                inputInt = in.nextInt();
                Vehicle v = Serialize.getVehicles().get(inputInt - 1000000);
                setVehicle(v);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                System.out.println("Wpiszcz prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }

        System.out.println("Podaj datę odbioru (w formacie DD/MM/YYYY)");
        inputDate();
        setStartDate(LocalDate.of(numbers[0], numbers[1], numbers[2]));

        System.out.println("Podaj datę dostawy (w formacie DD/MM/YYYY)");
        inputDate();
        setStartDate(LocalDate.of(numbers[0], numbers[1], numbers[2]));

        while (!done) {
            try {
                System.out.println("Podaj stawkę dla przewoźnika:");
                inputDouble = in.nextDouble();
                setCosts(inputDouble);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                System.out.println("Wpiszcz liczbę.");
                in = new Scanner(System.in);
            }
        }

        while (!done) {
            try {
                System.out.println("Podaj termin płatności:");
                inputInt = in.nextInt();
                setPaymentPeriod(inputInt);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                System.out.println("Wpiszcz liczbę.");
                in = new Scanner(System.in);
            }
        }

        System.out.println("Podaj rodzaj ładunku:");
        inputString = in.nextLine();
        this.setKindOfCargo(inputString);

        while (!done) {
            try {
                System.out.println("Podaj ilość km na trasie:");
                inputInt = in.nextInt();
                this.setKmPerTrip(inputInt);
                done = true;
            }catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                System.out.println("Wpisz liczbę");
                in = new Scanner(System.in);
            }
        }
        System.out.println("Dodatkowe informacje:");
        inputString = in.nextLine();
        setOtherRoute(inputString);
        Serialize.save();
        this.descriptionInstance();
    }

    @Override
    public void changeInstance() {
        this.descriptionInstance();
        System.out.println("Wybierz co chcesz zmienić:");
        System.out.printf("'1' - Zlecenie transportowe realizowane na trasie%n'2' - Spedytor obsługujący trasę%n'3' - Kierowca realizujący trasę%n'4' - Auto realizujące trasę%n'5' - Data załadunku%n'6' - Data rozładunku%n'7' - Termin płatności%n'8' - Rodzaj ładunku%n'9' - Pozostałe informacje%n'10' - Czy opłacono?%n'11' - Czy została wystawiona kara za trasę?%n'12' - Powrót do menu tras%n");
        try {
            Scanner in = new Scanner(System.in);
            int inputInt = in.nextInt();
            String inputString;
            boolean done = false;
            switch (inputInt) {
                case 1:
                    while (!done) {
                        try {
                            System.out.println("Wprowadź ID zlecenia transportowego:");
                            inputInt = in.nextInt();
                            this.setTransportOrder(Serialize.getTransportOrders().get(inputInt - 6000000));
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpiszcz prawidłowe ID.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 2:
                    while (!done) {
                        try {
                            System.out.println("Wprowadź ID spedytora:");
                            inputInt = in.nextInt();
                            this.setForwarder(Serialize.getForwarders().get(inputInt - 2000000));
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpiszcz prawidłowe ID.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 3:
                    while (!done) {
                        try {
                            System.out.println("Wprowadź ID kierowcy:");
                            inputInt = in.nextInt();
                            this.setDriver(Serialize.getDrivers().get(inputInt - 3000000));
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpiszcz prawidłowe ID.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 4:
                    while (!done) {
                        try {
                            System.out.println("Wprowadź ID auta:");
                            inputInt = in.nextInt();
                            this.setVehicle(Serialize.getVehicles().get(inputInt - 1000000));
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpiszcz prawidłowe ID.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 5:
                    System.out.println("Wprowadź datę załadunku (w formacie DD/MM/YYYY):");
                    inputDate();
                    this.setStartDate(LocalDate.of(getNumbers()[0], getNumbers()[1], getNumbers()[2]));
                    break;
                case 6:
                    System.out.println("Wprowadź datę rozładunku (w formacie DD/MM/YYYY):");
                    inputDate();
                    this.setEndDate(LocalDate.of(getNumbers()[0], getNumbers()[1], getNumbers()[2]));
                    break;
                case 7:
                    while (!done){
                        try {
                            System.out.println("Podaj termin płatności (w dniach):");
                            inputInt = in.nextInt();
                            this.setPaymentPeriod(inputInt);
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpisz liczbę.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 8:
                    System.out.println("Podaj rodzaj ładunku:");
                    inputString = in.nextLine();
                    this.setKindOfCargo(inputString);
                    break;
                case 9:
                    System.out.println("Podaj dodatkowe informacje:");
                    inputString = in.nextLine();
                    this.setOtherRoute(inputString);
                    break;
                case 10:
                    System.out.println("Czy przewoźnik został rozliczony za trasę?:");
                    System.out.println("Wpisz: 'TAK' lub 'NIE'");
                    inputString = in.nextLine();
                    while (!done) {
                        if (inputString.equalsIgnoreCase("TAK")) {
                            setPaid(true);
                            done = true;
                        } else if (inputString.equalsIgnoreCase("NIE")) {
                            setPaid(false);
                            done = true;
                        } else {
                            System.out.println("Wpisz 'TAK' lub 'NIE'");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 11:
                    System.out.println("Czy zostało wystawione obciążenie za trasę?");
                    System.out.println("Wpisz: 'TAK' lub 'NIE'");
                    inputString = in.nextLine();
                    while (!done) {
                        if (inputString.equalsIgnoreCase("TAK")) {
                            setPenaltY(true);
                            System.out.println("Podaj wysokość obciążenia:");
                            inputInt = in.nextInt();
                            setPenalty(inputInt);
                            System.out.println("Jaki jest powód wystawienia obciążenia?");
                            inputString = in.nextLine();
                            setReasonOfPenalty(inputString);
                            done = true;
                        } else if (inputString.equalsIgnoreCase("NIE")) {
                            setPenaltY(false);
                            setPenalty(0);
                            setReasonOfPenalty(null);
                            done = true;
                        } else {
                            System.out.println("Wpisz 'TAK' lub 'NIE'");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 12:
                    new RouteAssistant().routeMenu();
                    break;
                default:
                    System.out.println("Wybierz liczbę od 1 do 12.");
                    changeInstance();
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 12");
            this.changeInstance();
        }
        Serialize.save();
        this.descriptionInstance();
        this.changeInstance();
    }


    // metoda do pobierania daty od użytkownika
    public int[] inputDate() {
        Scanner in = new Scanner(System.in);
        String input = in.next();
        String a = null;
        String b = null;
        String c = null;
        int x = 0;
        int y = 0;
        int z = 0;
        boolean done = false;
        while (!done) {
            try {
                a = input.substring(0, 2);
                b = input.substring(3, 5);
                c = input.substring(6, 10);

                x = Integer.parseInt(a.trim());
                y = Integer.parseInt(b.trim());
                z = Integer.parseInt(c.trim());

                done = true;
            } catch (NumberFormatException | IndexOutOfBoundsException nfe) {
                System.out.println("Wpisz prawidłowy format daty: DD/MM/RRRR");
                in = new Scanner(System.in);
            }
        }
        int[] ymd = {z, y, x};
        return ymd;
    }

    @Override
    public String toString() {
        Signs.starsUp();

        System.out.print("Nr ID trasy: " + this.getIdRoute());
        System.out.print("Nr ID zlecenia transportowego realizowanego na trasie: " + this.getTransportOrder().getIdTransportOrder());
        System.out.print("Nr ID i nazwa spedytora spedytora obsługującego trasę: " + this.getForwarder().getIdEmployee() + " " + this.getForwarder().getName() + " " + getForwarder().getSurname());
        System.out.print("Nr ID i nazwa kierowcy realizującego trasę: " + this.getDriver().getIdEmployee() + " " + getDriver().getName() + " " + getDriver().getSurname());
        System.out.print("Nr ID i rejestracja auta realizującego zlecenie: " + this.getVehicle().getRegTruck() + "/" + getVehicle().getRegLorry());
        System.out.print("Nr ID i nazwa przewoźnika: " + this.getCarrier().getIdCompany() + " " + this.getCarrier().getNameOfCompany());
        System.out.print("Adres załadunku: " + this.getTransportOrder().getDirectionFrom());
        System.out.print("Adres rozładunku: " + this.getTransportOrder().getDirectionTo());
        System.out.print("Data załadunku: " + this.getStartDate());
        System.out.print("Data rozłądunku: " + this.getEndDate());
        System.out.print("Stawka dla przewoźnika: " + this.getCosts());
        System.out.print("Termin płatności: " + this.getPaymentPeriod());
        System.out.print("Ilość km na trasie: " + this.getKmPerTrip());
        System.out.print("Dodatkowe informacje: " + this.getOtherRoute());
        if (penaltY = true) {
            System.out.print(" ");
            System.out.print("Za trasę zostało wystawione obciążenie.");
            System.out.print("Wysokość obciążenia: " + getPenalty());
            System.out.print("Szczegóły dotyczące obciążenia: " + getReasonOfPenalty());
        }
        System.out.print(" ");
        System.out.print("Wynik finansowy trasy: " + (this.getTransportOrder().getPrice() - this.getCosts() - this.getPenalty()));

        Signs.starsBottom();
        return toString();
    }
}
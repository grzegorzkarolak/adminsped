package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.assistants.TransportOrderAssistant;
import com.GrzegorzKarolak.interfaces.Descriptive;
import com.GrzegorzKarolak.interfaces.Mutable;
import com.GrzegorzKarolak.main.Signs;
import com.GrzegorzKarolak.serialize.Serialize;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Scanner;

public class TransportOrder implements Serializable, Mutable, Descriptive {

    private String directionFrom;
    private String directionTo;
    private LocalDate dateCollection;
    private LocalDate dateDelivery;
    private Customer nameOfCustomer;
    private double price;
    private int paymentPeriod;
    private boolean isPaid;
    private String kindOfCargo;
    private int idTransportOrder;
    private static int countTransportOrder = 6000000;
    private String otherTransportOrder;
    private Forwarder forwarder;
    private int [] numbers;


    public String getDirectionFrom() {
        return directionFrom;
    }

    public void setDirectionFrom(String directionFrom) {
        this.directionFrom = directionFrom;
    }

    public String getDirectionTo() {
        return directionTo;
    }

    public void setDirectionTo(String directionTo) {
        this.directionTo = directionTo;
    }

    public LocalDate getDateCollection() {
        return dateCollection;
    }

    public void setDateCollection(LocalDate dateCollection) {
        this.dateCollection = dateCollection;
    }

    public LocalDate getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(LocalDate dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public Customer getNameOfCustomer() {
        return nameOfCustomer;
    }

    public void setNameOfCustomer(Customer nameOfCustomer) {
        this.nameOfCustomer = nameOfCustomer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(int paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getKindOfCargo() {
        return kindOfCargo;
    }

    public void setKindOfCargo(String kindOfCargo) {
        this.kindOfCargo = kindOfCargo;
    }

    public int getIdTransportOrder() {
        return idTransportOrder;
    }

    public void setIdTransportOrder(int idTransportOrder) {
        this.idTransportOrder = idTransportOrder;
    }

    public static int getCountTransportOrder() {
        return countTransportOrder;
    }

    public static void setCountTransportOrder(int countTransportOrder) {
        TransportOrder.countTransportOrder = countTransportOrder;
    }

    public String getOtherTransportOrder() {
        return otherTransportOrder;
    }

    public void setOtherTransportOrder(String otherTransportOrder) {
        this.otherTransportOrder = otherTransportOrder;
    }

    public Forwarder getForwarder() {
        return forwarder;
    }

    public void setForwarder(Forwarder forwarder) {
        this.forwarder = forwarder;
    }


    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public TransportOrder() {
        this.setIdTransportOrder(getCountTransportOrder());
        setCountTransportOrder(getCountTransportOrder()+1);
        Scanner in = new Scanner(System.in);
        int inputInt;
        double inputDouble;
        boolean done = false;
        while (!done) {
            try {
                System.out.println("Podaj ID zleceniodawcy:");
                inputInt = in.nextInt();
                this.setNameOfCustomer(Serialize.getCustomers().get(inputInt-5000000));
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.ArrayIndexOutOfBoundsException e){
                System.out.println("Podaj prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        done = false;
        while (!done) {
            try {
                System.out.println("Podaj ID spedytora obsługującego:");
                inputInt = in.nextInt();
                this.setForwarder(Serialize.getForwarders().get(inputInt-2000000));
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.ArrayIndexOutOfBoundsException e){
                System.out.println("Podaj prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        done = false;
        System.out.println("Podaj miejsce odbioru:");
        String inputStringO = in.nextLine();
        this.setDirectionFrom(inputStringO);

        //Czemu przeskakuje?? Czy tworzyć 1 zmienną danego typu i zmieniać przypisaną wartość czy dla każdej wartości oddzielną?

        System.out.println("Podaj miejsce dostawy:");
        String inputStringD = in.nextLine();
        this.setDirectionTo(inputStringD);
        System.out.println("Podaj datę odbioru (w formacie DD/MM/YYYY):");
        numbers = inputDate();
        this.setDateCollection(LocalDate.of(numbers[0], numbers[1], numbers[2]));
        System.out.println("Podaj datę dostawy (w formacie DD/MM/YYYY):");
        numbers = inputDate();
        this.setDateDelivery(LocalDate.of(numbers[0], numbers[1], numbers[2]));
        System.out.println("Podaj cenę za transport:");
        inputDouble = in.nextDouble();
        this.setPrice(inputDouble);
        System.out.println("Podaj termin płatności (w dniach):");
        inputInt = in.nextInt();
        this.setPaymentPeriod(inputInt);
        System.out.println("Podaj rodzaj ładunku");
        String inputStringL = in.nextLine();
        this.setKindOfCargo(inputStringL);
        System.out.println("Podaj pozostałe informacje:");
        inputStringO = in.nextLine();
        this.setOtherTransportOrder(inputStringO);
        System.out.println("Czy zlecenie zostało opłacone?:");
        System.out.println("Wpisz: 'TAK' lub 'NIE'");
        String inputStringYN = in.nextLine();
        while (!done) {
            if (inputStringYN.equalsIgnoreCase("TAK")) {
                setPaid(true);
                done = true;
            } else if (inputStringYN.equalsIgnoreCase("NIE")) {
                setPaid(false);
                done = true;
            } else {
                System.out.println("Wpisz 'TAK' lub 'NIE'");
                in = new Scanner(System.in);
            }
        }
        done = false;
        Serialize.save();
        descriptionInstance();
    }

    @Override
    public void changeInstance() {
        this.descriptionInstance();
        System.out.println("Wybierz co chcesz zmienić:");
        System.out.printf("'1' - Zleceniodawca%n'2' - Miejsce odbioru%n'3' - Miejsce dostawy%n'4' - Data odbioru%n'5' - Data dostawy%n'6' - Cena%n'7' - Termin płatności%n'8' - Rodzaj ładunku%n'9' - Pozostałe informacje%n'10' - Czy opłacono?%n'11' - Powrót do menu zleceń transportowych%n");
        try {
            Scanner in = new Scanner(System.in);
            int inputInt = in.nextInt();
            String inputString;
            boolean done = false;
            switch (inputInt) {
                case 1:
                    System.out.println("Wprowadź ID zleceniodawcy: ");
                    inputInt = in.nextInt();
                    this.setNameOfCustomer(Serialize.getCustomers().get(inputInt-6000));
                    break;
                case 2:
                    System.out.println("Podaj miejsce odbioru:");
                    inputString = in.nextLine();
                    this.setDirectionFrom(inputString);
                    break;
                case 3:
                    System.out.println("Podaj miejsce dostawy:");
                    inputString = in.nextLine();
                    this.setDirectionTo(inputString);
                    break;
                case 4:
                    //Czemu wpada w pętlę w przypadku błędu?

                    System.out.println("Podaj datę odbioru (w formacie DD/MM/YYYY):");
                    numbers = inputDate();
                    this.setDateCollection(LocalDate.of(numbers[0], numbers[1], numbers[2]));
                    break;
                case 5:
                    System.out.println("Podaj datę dostawy (w formacie DD/MM/YYYY):");
                    numbers = inputDate();
                    this.setDateDelivery(LocalDate.of(numbers[0], numbers[1], numbers[2]));
                    break;
                case 6:
                    System.out.println("Podaj cenę za transport:");
                    double inputDouble = in.nextDouble();
                    this.setPrice(inputDouble);
                    break;
                case 7:
                    System.out.println("Podaj termin płatności (w dniach):");
                    inputInt = in.nextInt();
                    this.setPaymentPeriod(inputInt);
                    break;
                case 8:
                    System.out.println("Podaj rodzaj ładunku");
                    inputString = in.nextLine();
                    this.setKindOfCargo(inputString);
                    break;
                case 9:
                    System.out.println("Podaj pozostałe informacje:");
                    inputString = in.nextLine();
                    this.setOtherTransportOrder(inputString);
                    break;
                case 10:
                    System.out.println("Czy transport został opłacony?:");
                    System.out.println("Wpisz: 'TAK' lub 'NIE'");
                    inputString = in.nextLine();
                    while (!done) {
                        if (inputString.equalsIgnoreCase("TAK")) {
                            setPaid(true);
                            done = true;
                        } else if (inputString.equalsIgnoreCase("NIE")) {
                            setPaid(false);
                            done = true;
                        } else {
                            System.out.println("Wpisz 'TAK' lub 'NIE'");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 11:
                    new TransportOrderAssistant().transportOrderMenu();
                    break;
                default:
                    System.out.println("Wybierz liczbę od 1 do 11.");
                    this.descriptionInstance();
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 6");
            this.changeInstance();
        }
        Serialize.save();
        this.descriptionInstance();
        this.changeInstance();
    }

    // metoda do pobierania daty od użytkownika
    public int[] inputDate() {
        Scanner inDate = new Scanner(System.in);
        String inputDate = inDate.nextLine();
        String a = null;
        String b = null;
        String c = null;
        int x = 0;
        int y = 0;
        int z = 0;
        boolean done = false;
        while (!done) {
            try {
                a = inputDate.substring(0, 2);
                b = inputDate.substring(3, 5);
                c = inputDate.substring(6, 10);

                x = Integer.parseInt(a.trim());
                y = Integer.parseInt(b.trim());
                z = Integer.parseInt(c.trim());

                done = true;
            } catch (NumberFormatException | IndexOutOfBoundsException nfe) {
                System.out.println("Wpisz prawidłowy format daty: DD/MM/RRRR");
                inputDate();
            }
        }
        int[] ymd = {z, y, x};
        return ymd;
    }

    @Override
    public String toString() {
        Signs.starsUp();

        System.out.print("Nr ID zlecenia transportowego: " + this.getIdTransportOrder());
        System.out.println("Zleceniodawca: " + this.getNameOfCustomer().getIdCompany() + " " + this.getNameOfCustomer().getNameOfCompany());
        System.out.println("Spedutor obsługujący: " + this.getForwarder().getIdEmployee() + " " + this.getForwarder().getName() + " " + this.getForwarder().getSurname());
        System.out.println("Miejsce odbioru: " + this.getDirectionFrom());
        System.out.println("Miejsce dostawy: " + this.getDirectionTo());
        System.out.println("Data odbioru: " +  this.getDateCollection());
        System.out.println("Data dostawy: " + this.getDateDelivery());
        System.out.println("Cena: " +  this.getPrice());
        System.out.println("Termin płatności: " +  this.getPaymentPeriod() + " dni");
        //dopisać nr trasy
        System.out.println("Rodzaj ładunku: " +  this.getKindOfCargo());
        System.out.println("Pozostałe informacje: " +  this.getOtherTransportOrder());
        System.out.println("Czy opłacono:");
        if (this.isPaid() == true) {
            System.out.println("TAK");
        } else {
            System.out.println("NIE");
        }

        Signs.starsBottom();
        return toString();
    }
}


package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.interfaces.Descriptive;
import com.GrzegorzKarolak.interfaces.Mutable;
import com.GrzegorzKarolak.main.Signs;

import java.io.Serializable;
import java.util.Scanner;

public abstract class Company implements Serializable, Mutable, Descriptive {

    private String nameOfCompany;
    private long nip;
    private boolean blackList;
    private String blackListWhy;
    private int idCompany;
    private static int countCarrier = 4000000;
    private static int countCustomer = 5000000;
    private String otherCompany;

    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public Company() {
        Signs.starsUp();
        setBlackList(false);
        setBlackListWhy(null);
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj nazwę firmy:");
        String inputString = in.nextLine();
        this.setNameOfCompany(inputString);
        long inputLong = 0;
        boolean done = false;
        while (!done) {
            try {
                System.out.println("Podaj NIP firmy");
                inputLong = in.nextLong();
                this.setNip(inputLong);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                System.out.println("Wpiszcz liczbę, bez dodatkowych znaków.");
                in = new Scanner(System.in);
            }
        }
    }

    public String getNameOfCompany() {
        return nameOfCompany;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public long getNip() {
        return nip;
    }

    public void setNip(long nip) {
        this.nip = nip;
    }

    public boolean isBlackList() {
        return blackList;
    }

    public void setBlackList(boolean blackList) {
        this.blackList = blackList;
    }

    public String getBlackListWhy() {
        return blackListWhy;
    }

    public void setBlackListWhy(String blackListWhy) {
        this.blackListWhy = blackListWhy;
    }

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    public static int getCountCarrier() {
        return countCarrier;
    }

    public static void setCountCarrier(int countCarrier) {
        Company.countCarrier = countCarrier;
    }

    public static int getCountCustomer() {
        return countCustomer;
    }

    public static void setCountCustomer(int countCustomer) {
        Company.countCustomer = countCustomer;
    }

    public String getOtherCompany() {
        return otherCompany;
    }

    public void setOtherCompany(String otherCompany) {
        this.otherCompany = otherCompany;
    }



    @Override
    public String toString() {
        Signs.starsUp();
        System.out.println("ID: " + this.getIdCompany());
        System.out.println("Nazwa: " + this.getNameOfCompany());
        System.out.println("NIP: " + this.getNip());
        System.out.print("Czy jest na 'czarnej liście': ");
        if (this.isBlackList() == true) {
            System.out.print("TAK");
        } else {
            System.out.print("NIE");
        }
        System.out.println(" ");
        if (this.isBlackList() == true) {
            System.out.println("Powód wpisania na 'czarną listę': " + getBlackListWhy());
        }
        return toString();
    }
}





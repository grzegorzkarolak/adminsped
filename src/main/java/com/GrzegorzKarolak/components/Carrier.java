package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.assistants.CarrierAssistant;
import com.GrzegorzKarolak.main.Signs;
import com.GrzegorzKarolak.serialize.Serialize;

import java.util.ArrayList;
import java.util.Scanner;

public class Carrier extends Company  {

    protected ArrayList<Driver> carrierDrivers = new ArrayList<>();
    protected ArrayList<Vehicle> carrierVehicles = new ArrayList<>();

    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public Carrier() {
        super();
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pozostałe informacje:");
        String inputS = in.nextLine();
        setOtherCompany(inputS);
        setIdCompany(Company.getCountCarrier());
        Company.setCountCarrier(Company.getCountCarrier() + 1);
        this.descriptionInstance();
        Serialize.save();
    }

    public void changeInstance() {
        this.descriptionInstance();
        System.out.println("Wybierz co chcesz zmienić:");
        System.out.printf("'1' - Nazwa%n'2' - NIP%n'3' - Dodanie na 'czarną listę'%n'4' - Usunięcie z 'czarnej listy'%n'5' - Dodatkowe informacje%n'6' - Powrót do menu Przewoźnicy%n");
        try {
            Scanner in = new Scanner(System.in);
            int inputInt = in.nextInt();
            String inputString;
            switch (inputInt) {
                case 1:
                    System.out.println("Wprowadź nową nazwę: ");
                    inputString = in.next();
                    this.setNameOfCompany(inputString);
                    break;
                case 2:
                    boolean done = false;
                    while (!done) {
                        try {
                            System.out.println("Wprowadź nowy NIP: ");
                            inputInt = in.nextInt();
                            this.setNip(inputInt);
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpiszcz liczbę, bez dodatkowych znaków.");
                        }
                    }
                    done = false;
                    break;
                case 3:
                    setBlackList(true);
                    System.out.println("Firma dodana na 'czarną listę'.");
                    System.out.println("Jaki jest powód?");
                    inputString = in.nextLine();
                    this.setBlackListWhy(inputString);
                    break;
                case 4:
                    setBlackList(false);
                    System.out.println("Firma usunięta z 'czarnej listy'.");
                    break;
                case 5:
                    System.out.println("Podaj dodatkowe informacje:");
                    inputString = in.next();
                    this.setOtherCompany(inputString);
                    break;
                case 6:
                    new CarrierAssistant().carrierMenu();
                default:
                    System.out.println("Wybierz liczbę od 1 do 6.");
                    this.changeInstance();
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 6");
            this.changeInstance();
        }
        Serialize.save();
        this.descriptionInstance();
        this.changeInstance();
    }



    @Override
    public String toString() {

        super.toString();
        System.out.println("Pozostałe informacje: " + getOtherCompany());

        Signs.starsUp();
        System.out.println("Lista kierowców: ");
        for (Driver i : this.carrierDrivers) {
            System.out.println("(" + i.getIdEmployee() + "): " + i.getName() + " " + i.getSurname());
        }
        Signs.starsUp();

        System.out.println("Lista aut: ");
        for (Vehicle j : this.carrierVehicles) {
            System.out.println("(" + j.getIdVehicle() + "): " + j.getRegTruck() + " / " + j.getRegLorry());
        }
        Signs.starsBottom();
        return toString();
    }
}






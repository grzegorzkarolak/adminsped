package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.assistants.ForwarderAssistant;
import com.GrzegorzKarolak.serialize.Serialize;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Forwarder extends Employee implements Serializable {

    ArrayList<Driver> forwarderDrivers = new ArrayList<>();


    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public Forwarder () {
        super();
        System.out.println("Pozostałe informacje: ");
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        this.setOtherEmployee(input);
        this.descriptionInstance();
        setIdEmployee(Employee.getCountForwarder());
        Employee.setCountForwarder(Employee.getCountForwarder()+1);
        this.descriptionInstance();
        Serialize.save();
    }



    @Override
    public void changeInstance() {
        this.descriptionInstance();
        System.out.println("Wybierz co chcesz zmienić:");
        System.out.printf("'1' - Imię%n'2' - Nazwisko%n'3' - Data zatrudnienia%n'4' - Data zwolnienia%n'5' - dodatkowe informacje%n'6' - Czy jest aktualnie zatrudniony%n'7' - powrót do menu Spedytor%n");
        try {
            Scanner in = new Scanner(System.in);
            int input = in.nextInt();
            String inputString;
            switch (input) {
                case 1:
                    System.out.println("Wprowadź nowe imię:");
                    inputString = in.next();
                    this.setName(inputString);
                    break;
                case 2:
                    System.out.println("Wprowadź nowe nazwisko:");
                    inputString = in.next();
                    this.setSurname(inputString);
                    break;
                case 3:
                    System.out.println("Wprowadź datę zatrudnienia:");
                    inputDate();
                    this.setHireDate(LocalDate.of(getNumbers()[0], getNumbers()[1], getNumbers()[2]));
                    break;
                case 4:
                    System.out.println("Wprowadź datę zwolnienia:");
                    inputDate();
                    this.setDismissialDate(LocalDate.of(getNumbers()[0], getNumbers()[1], getNumbers()[2]));
                    break;
                case 5:
                    System.out.println("Podaj dodatkowe informacje:");
                    inputString = in.next();
                    this.setOtherEmployee(inputString);
                    break;
                case 6:
                    System.out.println("Czy jest aktualnie zatrudniony:");
                    System.out.println("Wpisz: TAK lub NIE");
                    boolean done = false;
                    while (!done){
                        try {
                            inputString = in.next().toUpperCase();
                            com.GrzegorzKarolak.enums.YesOrNo yn = com.GrzegorzKarolak.enums.YesOrNo.valueOf(inputString);
                            if (yn == com.GrzegorzKarolak.enums.YesOrNo.TAK){
                                this.setInWork(true);
                            } else {
                                this.setInWork(false);
                            }
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpisz: 'TAK' lub 'NIE'");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 7:
                    new ForwarderAssistant().forwarderMenu();
                    break;
                default:
                    System.out.println("Wybierz liczbę od 1 do 7.");
                    changeInstance();
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 7");
            this.changeInstance();
        }
        Serialize.save();
        this.descriptionInstance();
        this.changeInstance();
    }

    @Override
    public String toString() {
        com.GrzegorzKarolak.main.Signs.starsUp();


        System.out.println("ID: " + this.getIdEmployee());
        System.out.println("Imię: " + this.getName());
        System.out.println("Nazwisko: " + this.getSurname());
        System.out.printf("Data zatrudnienia: " + this.getHireDate());
        if (this.getDismissialDate() != null) {
            System.out.printf("Data zwolnienia: " + this.getDismissialDate());
        }
        System.out.printf("%nCzy aktualnie pracuje ");
        if (this.isInWork() == true) {
            System.out.println("TAK");
        } else {
            System.out.println("NIE");
        }
        System.out.println("Pozostałe informacje: " + getOtherEmployee());


        com.GrzegorzKarolak.main.Signs.starsBottom();

        return toString();
    }

     {

    }
}










package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.interfaces.*;
import com.GrzegorzKarolak.enums.*;
import com.GrzegorzKarolak.serialize.Serialize;

import java.io.Serializable;
import java.util.Scanner;


public class Vehicle implements Serializable, Mutable, Descriptive {

    private String regTruck;
    private String regLorry;
    private Carrier owner;
    private double capacity;
    private int idVehicle;
    private boolean inUse = true;
    private String kindOfTruck;
    private String kindOfConstruction;
    private String other;
    private static int countVehicle = 1000000;


    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public Vehicle() {
        this.setIdVehicle(getCountVehicle());
        setCountVehicle(getCountVehicle()+1);
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj nr rejestracyjny auta: ");
        String input = in.nextLine().toUpperCase();
        this.setRegTruck(input);
        System.out.println("Podaj nr rejestracyjny naczepy: ");
        input = in.nextLine().toUpperCase();
        this.setRegLorry(input);
        boolean done = false;
        while (!done) {
            try {
                System.out.println("Podaj rodzaj ciężarówki. Wpisz:");
                System.out.printf("'A' - BUS %n'B' - FIRANKA %n'C' - NACZEPA %n'D' - WYWROTKA %n'E' - KONTENERY %n'F' - INNA%n");
                input = in.nextLine().toUpperCase();
                KindOfTruck truck = Enum.valueOf(KindOfTruck.class, input);
                this.setKindOfTruck(truck.getAbbreviation());
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e){
                System.out.println("Wpisz literę od 'A' do 'F'");
                in = new Scanner(System.in);
            }
        }
        done = false;
        while (!done) {
            try {
                System.out.println("Podaj rodzaj zabudowy. Wpisz:");
                System.out.printf("'A' - CHŁODNIA%n'B' - FIRANKA%n'C' - IZOTERMA%n'D' - PLANDEKA%n'E' - INNA%n");
                input = in.nextLine().toUpperCase();
                KindOfConstruction constr = Enum.valueOf(KindOfConstruction.class, input);
                this.setKindOfConstruction(constr.getAbbreviation());
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e){
                System.out.println("Wpisz literę od 'A' do 'E'");
                in = new Scanner(System.in);
            }
        }
        done = false;
        while (!done) {
            try {
                System.out.println("Podaj ID przewoźnika:");
                int inputInt = in.nextInt();
                this.setOwner(Serialize.getCarriers().get(inputInt-4000000));
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.ArrayIndexOutOfBoundsException e){
                System.out.println("Podaj prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        done = false;

        while (!done) {
            try {
                System.out.println("Podaj ładowność (w tonach):");
                double input2 = in.nextDouble();
                this.setCapacity(input2);
                done = true;
            }catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                System.out.println("Wpisz liczbę");
                in = new Scanner(System.in);
            }
        }
        done = false;
        System.out.println("Podaj dodatkowe informacje:");
        String input3 = in.nextLine();
        this.setOther(input3);

        for (Carrier c : Serialize.getCarriers()){
            if(this.getOwner().getIdCompany() == c.getIdCompany()){
                c.carrierVehicles.add(this);
            }
        }
        this.descriptionInstance();
    }



    public String getRegTruck() {
        return regTruck;
    }

    public String getRegLorry() {
        return regLorry;
    }

    public Carrier getOwner() {
        return owner;
    }

    public double getCapacity() {
        return capacity;
    }

    public int getIdVehicle() {
        return idVehicle;
    }


    public boolean isInUse() {
        return inUse;
    }


    public String getKindOfTruck() {
        return kindOfTruck;
    }

    public String getKindOfConstruction() {
        return kindOfConstruction;
    }

    public String getOther() {
        return other;
    }



    public void setRegTruck(String regTruck) {
        this.regTruck = regTruck;
    }

    public void setRegLorry(String regLorry) {
        this.regLorry = regLorry;
    }

    public void setOwner(Carrier owner) {
        this.owner = owner;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public void setIdVehicle(int idVehicle) {
        this.idVehicle = idVehicle;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }

    public void setKindOfTruck(String kindOfTruck) {
        this.kindOfTruck = kindOfTruck;
    }

    public void setKindOfConstruction(String kindOfConstruction) {
        this.kindOfConstruction = kindOfConstruction;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public static int getCountVehicle() {
        return countVehicle;
    }

    public static void setCountVehicle(int countVehicle) {
        Vehicle.countVehicle = countVehicle;
    }


    @Override
    public void changeInstance() {
        this.descriptionInstance();

        System.out.println("Wybierz co chcesz zmienić:");
        System.out.printf("'1' - Nr rejestracyjny auta%n'2' - Nr rejestracyjny naczepy%n'3' - Rodzaj auta%n'4' - Rodzaj zabudowy%n'5' - Ładowność%n'6' - dodatkowe informacje%n'7' - Czy jest w użyciu%n");
        try {
            Scanner in = new Scanner(System.in);
            int input = in.nextInt();
            String input2;
            boolean done = false;
            switch (input) {
                case 1:
                    System.out.println("Wprowadź nowy numer rejestracyjny auta:");
                    input2 = in.nextLine();
                    this.setRegTruck(input2);
                    break;
                case 2:
                    System.out.println("Wprowadź nowy numer rejestracyjny naczepy:");
                    input2 = in.nextLine();
                    this.setRegLorry(input2);
                    break;
                case 3:
                    while (!done){
                        try {
                            System.out.println("Wprowadź nowy rodzaj ciężarówki:");
                            System.out.printf("'A' - BUS %n'B' - FIRANKA %n'C' - NACZEPA %n'D' - WYWROTKA %n'E' - KONTENERY %n'F' - INNA%n");
                            input2 = in.next().toUpperCase();
                            KindOfTruck truck = Enum.valueOf(KindOfTruck.class, input2);
                            this.setKindOfTruck(truck.getAbbreviation());
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wybierz literę od 'A' do 'F'");
                            in = new Scanner(System.in);
                        }
                    }
                    break;
                case 4:
                    while (!done){
                        try {
                            System.out.println("Wprowadź nowy rodzaj zabudowy:");
                            System.out.printf("'A' - CHŁODNIA%n'B' - FIRANKA%n'C' - IZOTERMA%n'D' - PLANDEKA%n'E' - INNA%n");
                            input2 = in.next().toUpperCase();
                            KindOfConstruction constr = Enum.valueOf(KindOfConstruction.class, input2);
                            this.setKindOfConstruction(constr.getAbbreviation());
                            done = true;
                        }catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wybierz literę od 'A' do 'E'");
                            in = new Scanner(System.in);
                        }
                    }
                    break;
                case 5:
                    while (!done){
                        try {
                            System.out.println("Podaj nową ładowność (w tonach):");
                            double input3 = in.nextDouble();
                            this.setCapacity(input3);
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpisz liczbę");
                            in = new Scanner(System.in);
                        }
                    }
                    break;
                case 6:
                    System.out.println("Podaj dodatkowe informacje:");
                    input2 = in.next();
                    this.setOther(input2);
                    break;
                case 7:
                    while (!done){
                        try {
                            System.out.println("Czy auto jest w użyciu:");
                            System.out.println("Wpisz: TAK lub NIE");
                            input2 = in.next().toUpperCase();
                            YesOrNo yn = YesOrNo.valueOf(input2);
                            if (yn == YesOrNo.TAK){
                                this.setInUse(true);
                            } else {
                                this.setInUse(false);
                            }
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wybierz literę od 'A' do 'E'");
                            in = new Scanner(System.in);
                        }
                    }
                    break;
                default:
                    System.out.println("Wybierz liczbę od 1 do 7.");
                    changeInstance();
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 7");
            this.changeInstance();
        }

        this.descriptionInstance();
        this.changeInstance();
    }

    @Override
    public String toString() {
        com.GrzegorzKarolak.main.Signs.starsUp();

        System.out.println(" ");

        System.out.println("Nr ID auta: " + getIdVehicle());
        System.out.println("Nr rejestracyjny auta: " + this.getRegTruck());
        System.out.println("Nr rejestracyjny naczepy: " + this.getRegLorry());
        System.out.println("Rodzaj auta: " +  this.getKindOfTruck());
        System.out.println("Rodzaj zabudowy: " + this.getKindOfConstruction());
        System.out.println("Przewoźnik: " +  this.getOwner().getIdCompany() + " " + this.getOwner().getNameOfCompany());
        System.out.println("Ładowność: " +  this.getCapacity() + " T");
        System.out.println("W użyciu:");
        if (this.isInUse() == true) {
            System.out.println("TAK");
        } else {
            System.out.println("NIE");
        }
        System.out.println("Dodatkowe informacje: " + this.getOther());

        com.GrzegorzKarolak.main.Signs.starsBottom();
        return toString();

    }
}










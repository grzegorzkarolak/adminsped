package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.assistants.DriverAssistant;
import com.GrzegorzKarolak.main.*;
import com.GrzegorzKarolak.serialize.Serialize;
import com.GrzegorzKarolak.enums.YesOrNo;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Scanner;

// podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
public class Driver extends Employee implements Serializable {

    private double totalKm;
    private Forwarder admin;
    private Carrier employer;
    private Vehicle auto;

    public double getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(double totalKm) {
        this.totalKm = totalKm;
    }

    public Forwarder getAdmin() {
        return admin;
    }

    public void setAdmin(Forwarder admin) {
        this.admin = admin;
    }

    public Carrier getEmployer() {
        return employer;
    }

    public void setEmployer(Carrier employer) {
        this.employer = employer;
    }

    public Vehicle getAuto() {
        return auto;
    }

    public void setAuto(Vehicle auto) {
        this.auto = auto;
    }

    public Driver() {
        super();
        boolean done = false;
        Scanner in = new Scanner(System.in);
        int input = 0;
        while (!done) {
            try {
                System.out.println("Podaj ID spedytora kierowcy:");
                input = in.nextInt();
                Forwarder f = Serialize.getForwarders().get(input - 2000000);
                setAdmin(f);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                System.out.println("Wpiszcz prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        done = false;
        while (!done) {
            try {
                System.out.println("Podaj ID przewoźnika kierowcy:");
                input = in.nextInt();
                Carrier c = Serialize.getCarriers().get(input - 4000000);
                setEmployer(c);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                System.out.println("Wpiszcz prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        done = false;
        while (!done) {
            try {
                System.out.println("Podaj ID auta przypisanego kierowcy:");
                input = in.nextInt();
                Vehicle v = Serialize.getVehicles().get(input - 1000000);
                setAuto(v);
                done = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                System.out.println("Wpiszcz prawidłowe ID.");
                in = new Scanner(System.in);
            }
        }
        done = false;
        System.out.println("Podaj pozostałe informacje:");
        String inputS = in.next();
        setOtherEmployee(inputS);
        setIdEmployee(Employee.getCountDriver());
        Employee.setCountDriver(Employee.getCountDriver() + 1);

        for (Forwarder fo : Serialize.getForwarders()) {
            if (this.getAdmin().getIdEmployee() == fo.getIdEmployee()) {
                fo.forwarderDrivers.add(this);
            }
        }

        for (Carrier ca : Serialize.getCarriers()) {
            if (this.getEmployer().getIdCompany() == ca.getIdCompany()) {
                ca.carrierDrivers.add(this);
            }
        }
        Serialize.save();
        this.descriptionInstance();
    }

    @Override
    public void changeInstance() {
        this.descriptionInstance();
        System.out.println("Wybierz co chcesz zmienić:");
        System.out.printf("'1' - Imię%n'2' - Nazwisko%n'3' - Data zatrudnienia%n'4' - Data zwolnienia%n'5' - dodatkowe informacje%n'6' - Czy jest aktualnie zatrudniony%n'7' - Spedytor - opiekun%n'8' - Przewoźnik%n'9' - Pojazd%n'10' - Powrót do menu kierowców%n");
        try {
            Scanner in = new Scanner(System.in);
            int input = in.nextInt();
            String inputString;
            boolean done = false;
            switch (input) {
                case 1:
                    System.out.println("Wprowadź nowe imię:");
                    inputString = in.next();
                    this.setName(inputString);
                    break;
                case 2:
                    System.out.println("Wprowadź nowe nazwisko:");
                    inputString = in.next();
                    this.setSurname(inputString);
                    break;
                case 3:
                    System.out.println("Wprowadź datę zatrudnienia:");
                    inputDate();
                    this.setHireDate(LocalDate.of(getNumbers()[0], getNumbers()[1], getNumbers()[2]));
                    break;
                case 4:
                    System.out.println("Wprowadź datę zwolnienia:");
                    inputDate();
                    this.setDismissialDate(LocalDate.of(getNumbers()[0], getNumbers()[1], getNumbers()[2]));
                    break;
                case 5:
                    System.out.println("Podaj dodatkowe informacje:");
                    inputString = in.next();
                    this.setOtherEmployee(inputString);
                    break;
                case 6:
                    System.out.println("Czy jest aktualnie zatrudniony:");
                    System.out.println("Wpisz: TAK lub NIE");
                    while (!done){
                        try {
                            inputString = in.next().toUpperCase();
                            YesOrNo yn = YesOrNo.valueOf(inputString);
                            if (yn == YesOrNo.TAK){
                                this.setInWork(true);
                            } else {
                                this.setInWork(false);
                            }
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpisz: 'TAK' lub 'NIE'");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 7:
                    while (!done) {
                        try {
                            System.out.println("Wprowadź ID nowego spedytora:");
                            input = in.nextInt();
                            this.setAdmin(Serialize.getForwarders().get(input - 2000000));
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                            System.out.println("Wpiszcz prawidłowe ID spedytora.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 8:
                    while (!done) {
                        try {
                            System.out.println("Wprowadź ID nowego przewoźnika:");
                            input = in.nextInt();
                            this.setEmployer(Serialize.getCarriers().get(input - 4000000));
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException | java.lang.IndexOutOfBoundsException e) {
                            System.out.println("Wpiszcz prawidłowe ID przewoźnika.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 9:
                    while (!done) {
                        try {
                            System.out.println("Wprowadź ID nowego pojazdu:");
                            input = in.nextInt();
                            this.setAuto(Serialize.getVehicles().get(input - 1000000));
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpiszcz prawidłowe ID przewoźnika.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 10:
                    new DriverAssistant().driverMenu();
                    break;
                default:
                    System.out.println("Wybierz liczbę od 1 do 10.");
                    changeInstance();
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 6");
            this.changeInstance();
        }
        Serialize.save();
        this.descriptionInstance();
        this.changeInstance();
    }

    @Override
    public String toString() {
        Signs.starsUp();

        System.out.println("ID: " + this.getIdEmployee());
        System.out.println("Imię: " + this.getName());
        System.out.println("Nazwisko: " + this.getSurname());
        System.out.printf("Data zatrudnienia: " + this.getHireDate());
        if (this.getDismissialDate() != null) {
            System.out.printf("Data zwolnienia: " + this.getDismissialDate());
        }
        System.out.printf("%nCzy aktualnie pracuje ");
        if (this.isInWork() == true) {
            System.out.println("TAK");
        } else {
            System.out.println("NIE");
        }
        System.out.println("Ilość KM: " + this.getTotalKm());
        System.out.println("Spedytor (ID): " + this.getAdmin().getName() + " " + this.getAdmin().getSurname() + " (" + this.getAdmin().getIdEmployee() + ")");
        System.out.println("Przewoźnik (ID): " + this.getEmployer().getNameOfCompany() + " (" + this.getEmployer().getIdCompany() + ")");
        System.out.println("Pojazd (ID): " + this.getAuto().getRegTruck() + " / " + this.getAuto().getRegLorry());
        System.out.println("Pozostałe: " + this.getOtherEmployee());

        Signs.starsBottom();
        return toString();
    }
}



package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.interfaces.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Scanner;

public abstract class Employee implements Serializable, Mutable, Descriptive {

    private int idEmployee;
    private static int countForwarder = 2000000;
    private static int countDriver = 3000000;

    private String name;
    private String surname;
    private LocalDate hireDate;
    private LocalDate dismissialDate;
    private boolean inWork = true;
    private String otherEmployee;
    private int [] numbers;


    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public Employee() {
        com.GrzegorzKarolak.main.Signs.starsUp();
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj imię:");
        String input = in.nextLine();
        this.setName(input);
        System.out.println("Podaj nazwisko:");
        input = in.nextLine();
        this.setSurname(input);
        System.out.println("Podaj datę zatrudnienia (w formacie DD/MM/YYYY)");
        numbers = inputDate();
        this.setHireDate(LocalDate.of(numbers[0], numbers[1], numbers[2]));
        this.setInWork(true);

    }



    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }


    public void setHireDate(LocalDate hireDate) {

        this.hireDate = hireDate;
    }

    public LocalDate getDismissialDate() {
        return dismissialDate;
    }

    public void setDismissialDate(LocalDate dismissialDate) {
        this.dismissialDate = dismissialDate;
    }

    public boolean isInWork() {
        return inWork;
    }

    public void setInWork(boolean inWork) {
        this.inWork = inWork;
    }

    public String getOtherEmployee() {
        return otherEmployee;
    }

    public void setOtherEmployee(String otherEmployee) {
        this.otherEmployee = otherEmployee;
    }

    public int[] getNumbers() {
        return numbers;
    }


    public static int getCountForwarder() {
        return countForwarder;
    }

    public static void setCountForwarder(int countForwarder) {
        Employee.countForwarder = countForwarder;
    }

    public static int getCountDriver() {
        return countDriver;
    }

    public static void setCountDriver(int countDriver) {
        Employee.countDriver = countDriver;
    }




    // metoda do pobierania daty od użytkownika
    public int[] inputDate() {
        Scanner inDate = new Scanner(System.in);
        String inputDate = inDate.nextLine();
        String a = null;
        String b = null;
        String c = null;
        int x = 0;
        int y = 0;
        int z = 0;
        boolean done = false;
        while (!done) {
            try {
                a = inputDate.substring(0, 2);
                b = inputDate.substring(3, 5);
                c = inputDate.substring(6, 10);

                x = Integer.parseInt(a.trim());
                y = Integer.parseInt(b.trim());
                z = Integer.parseInt(c.trim());

                done = true;
            } catch (NumberFormatException | IndexOutOfBoundsException nfe) {
                System.out.println("Wpisz prawidłowy format daty: DD/MM/RRRR");
                inDate = new Scanner(System.in);
            }
        }
        int[] ymd = {z, y, x};
        return ymd;
    }







}

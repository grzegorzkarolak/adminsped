package com.GrzegorzKarolak.components;

import com.GrzegorzKarolak.assistants.CustomerAssistant;
import com.GrzegorzKarolak.main.Signs;
import com.GrzegorzKarolak.serialize.Serialize;

import java.util.Scanner;

public class Customer extends Company {


    // podczas tworzenia nowego obiektu nadawane jest ID automatycznie oraz określane są właściwości
    public Customer()
    {
        super();
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pozostałe informacje:");
        String inputS = in.nextLine();
        setOtherCompany(inputS);
        setIdCompany(Company.getCountCustomer());
        Company.setCountCustomer(Company.getCountCustomer()+1);
        this.descriptionCompany();
        Serialize.save();
    }


    @Override
    public void changeInstance() {
        this.descriptionCompany();
        System.out.println("Wybierz co chcesz zmienić:");
        System.out.printf("'1' - Nazwa%n'2' - NIP%n'3' - Dodanie na 'czarną listę'%n'4' - Usunięcie z 'czarnej listy'%n'5' - Dodatkowe informacje%n'6' - Powrót do menu Klienci%n");
        try {
            Scanner in = new Scanner(System.in);
            int inputInt = in.nextInt();
            String inputString;
            switch (inputInt) {
                case 1:
                    System.out.println("Wprowadź nową nazwę: ");
                    inputString = in.next();
                    this.setNameOfCompany(inputString);
                    break;
                case 2:
                    boolean done = false;
                    while (!done){
                        try {
                            System.out.println("Wprowadź nowy NIP: ");
                            long inputLong = in.nextLong();
                            this.setNip(inputLong);
                            done = true;
                        } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                            System.out.println("Wpiszcz liczbę, bez dodatkowych znaków.");
                            in = new Scanner(System.in);
                        }
                    }
                    done = false;
                    break;
                case 3:
                    setBlackList(true);
                    System.out.println("Firma dodana na 'czarną listę'.");
                    System.out.println("Jaki jest powód?");
                    inputString = in.next();
                    this.setBlackListWhy(inputString);
                    break;
                case 4:
                    setBlackList(false);
                    System.out.println("Firma usunięta z 'czarnej listy'.");
                    break;
                case 5:
                    System.out.println("Podaj dodatkowe informacje:");
                    inputString = in.next();
                    this.setOtherCompany(inputString);
                    break;
                case 6:
                    new CustomerAssistant().customerMenu();
                    break;
                default:
                    System.out.println("Wybierz liczbę od 1 do 6.");
                    this.changeInstance();
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 6");
            this.changeInstance();
        }
        Serialize.save();
        this.descriptionCompany();
        this.changeInstance();
    }

    public void descriptionCompany() {

        }

    @Override
    public String toString() {
        super.descriptionInstance();
        System.out.println("Pozostałe informacje: " + getOtherCompany());

        Signs.starsBottom();
        return "Customer{}";
    }
}


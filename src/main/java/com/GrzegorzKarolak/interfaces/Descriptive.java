package com.GrzegorzKarolak.interfaces;

public interface Descriptive {

    //  interfejs określający sposób reprezentacji klas, które zaimplementowały interfejs
    public default void descriptionInstance() {
        toString();
    }
}

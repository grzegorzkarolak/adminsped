package com.GrzegorzKarolak.interfaces;

public interface Mutable {

    // interfejs zaimplementowany przez klasy, które mają opcję zmiany swoich pól
    void changeInstance();
    
}

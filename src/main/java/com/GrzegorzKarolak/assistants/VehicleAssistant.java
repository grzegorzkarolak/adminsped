package com.GrzegorzKarolak.assistants;

import com.GrzegorzKarolak.main.Menu;

import java.util.Scanner;

//  dziedziczy metody po Assistant
public class VehicleAssistant extends Assistant {

    //  menu klasy Vehicle; metoda wywoływana przez mainManu
    public void vehicleMenu() {
        System.out.printf("%nWybierz co chcesz zrobić:%n");
        System.out.printf("'1' - Dodanie auta%n'2' - Zmiana danych auta%n'3' - Informacje o aucie%n'4' - Wyświetl wszystkie auta%n'5' - Powrót do głównego menu%n");
        try {
            Scanner in = new Scanner(System.in);
            int input = in.nextInt();
            switch (input) {
                case 1:
                    addInstanceMenu(this);
                    break;

                case 2:
                    changeInstanceMenu(this);
                    break;

                case 3:
                    descriptionInstanceMenu(this);
                    break;

                case 4:
                    showAllInstances(this);
                    break;

                case 5:
                    Menu.mainMenu();
                    break;

                default:
                    System.out.println("Wybierz nr od 1 do 5");
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 5");
            vehicleMenu();
        }
        vehicleMenu();


    }


}



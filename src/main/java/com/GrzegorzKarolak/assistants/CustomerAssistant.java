package com.GrzegorzKarolak.assistants;

import com.GrzegorzKarolak.main.Menu;

import java.util.Scanner;

/**
 * dziedziczy metody po Assistant
 */
public class CustomerAssistant extends Assistant {

    /**
     *  menu klasy Customer; metoda wywoływana przez mainManu
     */
    public void customerMenu() {
        System.out.printf("%nWybierz co chcesz zrobić:%n");
        System.out.printf("'1' - Dodanie klienta%n'2' - Zmiana danych klienta%n'3' - Informacje o kliencie%n'4' - Wyświetl wszystkich klientów%n'5' - Powrót do głównego menu%n");
        try {
            Scanner in = new Scanner(System.in);
            int inputInt = in.nextInt();
            switch (inputInt) {
                case 1:
                    addInstanceMenu(this);
                    break;

                case 2:
                    changeInstanceMenu(this);
                    break;

                case 3:
                    descriptionInstanceMenu(this);
                    break;

                case 4:
                    showAllInstances(this);
                    break;

                case 5:
                    Menu.mainMenu();
                    break;

                default:
                    System.out.println("Wybierz nr od 1 do 5");
                    break;
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Wybierz nr od 1 do 5"); //TODO: do poprawy w zadaniu Task-003
            customerMenu();
        }

        customerMenu();
    }
}

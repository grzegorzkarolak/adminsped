package com.GrzegorzKarolak.assistants;

import com.GrzegorzKarolak.components.*;

import java.util.Scanner;

import static com.GrzegorzKarolak.serialize.Serialize.*;

/**
 * klasa dostarczająca metody obsługujące
 * menu dla każdego z obiektów (XxxxAssistant - menu obiektu Xxxx)
 */
public abstract class Assistant {

    //  metoda jest wywoływana przy żądaniu stworzenia nowego obiektu
    public void addInstanceMenu(Assistant a) {
        if (a instanceof VehicleAssistant) {
            getVehicles().add(new Vehicle());
        } else if (a instanceof ForwarderAssistant) {
            getForwarders().add(new Forwarder());
        } else if (a instanceof DriverAssistant) {
            getDrivers().add(new Driver());
        } else if (a instanceof CarrierAssistant) {
            getCarriers().add(new Carrier());
        } else if (a instanceof CustomerAssistant) {
            getCustomers().add(new Customer());
        } else if (a instanceof TransportOrderAssistant) {
            getTransportOrders().add(new TransportOrder());
        } else if (a instanceof RouteAssistant) {
            getRoutes().add(new Route());
        }
    }

    /**
     * metoda jest wywowyłana przy żądniu dokonania zmian w obiekcie
     * @param a
     */
    public void changeInstanceMenu(Assistant a) {

        boolean doneChange = false;
        while (!doneChange) {
            try {
                Scanner inChange = new Scanner(System.in);
                System.out.printf("Podaj ID:%n");
                int inputChange = inChange.nextInt();

                if (a instanceof VehicleAssistant) {
                    Vehicle vc = getVehicles().get(inputChange - 1000000);
                    vc.changeInstance();
                    vc.descriptionInstance();

                } else if (a instanceof ForwarderAssistant) {
                    Forwarder f = getForwarders().get(inputChange - 2000000);
                    f.changeInstance();
                    f.descriptionInstance();

                } else if (a instanceof DriverAssistant) {
                    Driver d = getDrivers().get(inputChange - 3000000);
                    d.changeInstance();
                    d.descriptionInstance();

                } else if (a instanceof CarrierAssistant) {
                    Carrier c = getCarriers().get(inputChange - 4000000);
                    c.changeInstance();
                    c.descriptionInstance();

                } else if (a instanceof CustomerAssistant) {
                    Customer cu = getCustomers().get(inputChange - 5000000);
                    cu.changeInstance();
                    cu.descriptionInstance();

                } else if (a instanceof TransportOrderAssistant) {
                    TransportOrder to = getTransportOrders().get(inputChange - 6000000);
                    to.changeInstance();
                    to.descriptionInstance();

                } else if (a instanceof RouteAssistant) {
                    Route r = getRoutes().get(inputChange - 7000000);
                    r.changeInstance();
                    r.descriptionInstance();
                }


                doneChange = true;
            } catch (java.lang.IllegalArgumentException | java.util.InputMismatchException e) {
                System.out.println("Wpisz prawidłowe ID.");
                changeInstanceMenu(a);
            }
        }
    }


        /**
         * metoda jest wywoływana przy woborze opisu obiektu
         * @param a przyjmuje parametr typu Assistant
         */
    public void descriptionInstanceMenu(Assistant a) {
        if (a instanceof VehicleAssistant) {
            boolean doneDescript = false;
            while (!doneDescript) {
                try {
                    Scanner inDescription = new Scanner(System.in);
                    System.out.printf("Podaj ID:%n");
                    int inputDescription = inDescription.nextInt();

                    if (a instanceof VehicleAssistant) {
                        Vehicle vi = getVehicles().get(inputDescription - 1000000);
                        vi.descriptionInstance();

                    } else if (a instanceof ForwarderAssistant) {
                        Forwarder f = getForwarders().get(inputDescription - 2000000);
                        f.descriptionInstance();

                    } else if (a instanceof DriverAssistant) {
                        Driver d = getDrivers().get(inputDescription - 3000000);
                        d.descriptionInstance();

                    } else if (a instanceof CarrierAssistant) {
                        Carrier c = getCarriers().get(inputDescription - 4000000);
                        c.descriptionInstance();

                    } else if (a instanceof CustomerAssistant) {
                        Customer cu = getCustomers().get(inputDescription - 5000000);
                        cu.descriptionInstance();

                    } else if (a instanceof TransportOrderAssistant) {
                        TransportOrder to = getTransportOrders().get(inputDescription - 6000000);
                        to.descriptionInstance();

                    } else if (a instanceof RouteAssistant) {
                        Route to = getRoutes().get(inputDescription - 7000000);
                        to.descriptionInstance();
                    }

                    doneDescript = true;
                } catch (Exception e) {
                    System.out.println("Wpiszcz prawidłowe ID.");
                    descriptionInstanceMenu(a);
                }
            }
        }
    }

    // wyświetlenie wszystkich stworzonych obiektów danego rodzaju
    public void showAllInstances(Assistant a) {

        if (a instanceof VehicleAssistant) {
            for (Vehicle i : getVehicles()) {
                i.descriptionInstance();
            }

        } else if (a instanceof ForwarderAssistant) {
            for (Forwarder i : getForwarders()) {
                i.descriptionInstance();
            }

        } else if (a instanceof DriverAssistant) {
            for (Driver i : getDrivers()) {
                i.descriptionInstance();
            }

        } else if (a instanceof CarrierAssistant) {
            for (Carrier i : getCarriers()) {
                i.descriptionInstance();
            }

        } else if (a instanceof CustomerAssistant) {
            for (Customer i : getCustomers()) {
                i.descriptionInstance();
            }

        } else if (a instanceof TransportOrderAssistant) {
            for (TransportOrder i : getTransportOrders()) {
                i.descriptionInstance();
            }

        } else if (a instanceof RouteAssistant) {
            for (Route i : getRoutes()) {
                i.descriptionInstance();
            }
        }
    }
}





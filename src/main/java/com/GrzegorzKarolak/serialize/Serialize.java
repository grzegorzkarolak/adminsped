package com.GrzegorzKarolak.serialize;

import com.GrzegorzKarolak.components.*;

import java.io.*;
import java.util.ArrayList;



//  Klasa do serializowania stanu programu
public class Serialize {

    //  Klasa statyczna - brak możliwości utworzenia obiektu
    private Serialize() { }


    //  Listy przechowujące referencje do utworzonyh obiektów
    private static ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
    private static ArrayList<Forwarder> forwarders = new ArrayList<Forwarder>();
    private static ArrayList<Driver> drivers = new ArrayList<Driver>();
    private static ArrayList<Carrier> carriers = new ArrayList<Carrier>();
    private static ArrayList<Customer> customers = new ArrayList<Customer>();
    private static ArrayList<TransportOrder> transportOrders = new ArrayList<TransportOrder>();
    private static ArrayList<Route> routes = new ArrayList<Route>();
//    private static ArrayList<Users> users = new ArrayList<Users>();


    public static ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    public static void setVehicles(ArrayList<Vehicle> vehicles) {
        Serialize.vehicles = vehicles;
    }

    public static ArrayList<Forwarder> getForwarders() {
        return forwarders;
    }

    public static void setForwarders(ArrayList<Forwarder> forwarders) {
        Serialize.forwarders = forwarders;
    }

    public static ArrayList<Driver> getDrivers() {
        return drivers;
    }

    public static void setDrivers(ArrayList<Driver> drivers) {
        Serialize.drivers = drivers;
    }

    public static ArrayList<Carrier> getCarriers() {
        return carriers;
    }

    public static void setCarriers(ArrayList<Carrier> carriers) {
        Serialize.carriers = carriers;
    }

    public static ArrayList<Customer> getCustomers() {
        return customers;
    }

    public static void setCustomers(ArrayList<Customer> customers) {
        Serialize.customers = customers;
    }

    public static ArrayList<TransportOrder> getTransportOrders() {
        return transportOrders;
    }

    public static void setTransportOrders(ArrayList<TransportOrder> transportOrders) {
        Serialize.transportOrders = transportOrders;
    }

    public static ArrayList<Route> getRoutes() {
        return routes;
    }

    public static void setRoutes(ArrayList<Route> routes) {
        Serialize.routes = routes;
    }

//    public static ArrayList<Users> getUsers() {
//        return users;
//    }
//
//    public static void setUsers(ArrayList<Users> users) {
//        Main.users = users;
//    }





    // Metoda zapisująca stan programu
    public static void save(){
        try {

            // utworzenie listy 'save', a następnie "spakowanie" do niej list ze wszystkimi obiektami programu
            ArrayList save = new ArrayList();
            save.add(getVehicles());
            save.add(getForwarders());
            save.add(getDrivers());
            save.add(getCarriers());
            save.add(getCustomers());
            save.add(getTransportOrders());
//            save.add(getUsers());

            FileOutputStream fos = new FileOutputStream("data.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(save);
            oos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Metoda odczytująca stan programu
    public static void load() {
        try {

            FileInputStream fis = new FileInputStream("data.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);

            //  "Odpakowanie" serlializowanych obiektów
            ArrayList result = (ArrayList) ois.readObject();

            setVehicles((ArrayList<Vehicle>) result.get(0));
            setForwarders((ArrayList<Forwarder>) result.get(1));
            setDrivers((ArrayList<Driver>) result.get(2));
            setCarriers((ArrayList<Carrier>) result.get(3));
            setCustomers((ArrayList<Customer>) result.get(4));
            setTransportOrders((ArrayList<TransportOrder>) result.get(5));
//            setUsers((ArrayList<Users>) result.get(6));

            ois.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

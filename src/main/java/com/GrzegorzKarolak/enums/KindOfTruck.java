package com.GrzegorzKarolak.enums;

//  enum określający rodzaje zabudwoy aut
public enum KindOfTruck {
    A("BUS"),
    B("SOLÓWKA"),
    C("NACZEPA"),
    D("WYWROTKA"),
    E("KONTENERY"),
    F("INNA");

    private String abbreviation;

    private KindOfTruck(String abbreviation) { this.abbreviation = abbreviation; }
    public String getAbbreviation() { return abbreviation; }
}

package com.GrzegorzKarolak.enums;

//  enum określający rodzaj zabudopwy aut
public enum KindOfConstruction {
    A("CHŁODNIA"),
    B("FIRANKA"),
    C("IZOTERMA"),
    D("PLANDEKA"),
    E("INNA");

    private String abbreviation;

    private KindOfConstruction(String abbreviation) { this.abbreviation = abbreviation; }

    public String getAbbreviation() { return abbreviation; }
}
